function DummyPaymentProvider() {
  this.description = "Dummy Payment Provider";
};

DummyPaymentProvider.prototype.pay = function pay(amount) {
  console.log("Paying", amount);
  console.log("Paid");
  return true;
};

module.exports = DummyPaymentProvider;
