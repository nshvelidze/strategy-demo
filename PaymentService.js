function PaymentService() {
  this.providers = {};
};

PaymentService.prototype.addProvider = function(name, provider) {
  this.providers[name] = provider;
};

PaymentService.prototype.pay = function(amount, provider) {
  return this.providers[provider].pay(amount);
};

module.exports = new PaymentService();
