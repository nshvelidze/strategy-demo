function WalletPaymentProvider(balance) {
  this.balance = balance;
};

WalletPaymentProvider.prototype.pay = function pay(amount) {
  console.log("Paying", amount, "from balance", this.balance)
  if(this.balance >= amount) {
    console.log("Paid");
    this.balance -= amount;
    return true;
  } else {
    console.error("Not enough balance");
    return false;
  }
};

module.exports = WalletPaymentProvider;
