var PaymentService = require('./PaymentService'),
    DummyPaymentProvider = require('./DummyPaymentProvider'),
    WalletPaymentProvider = require('./WalletPaymentProvider');

PaymentService.addProvider('dummy', new DummyPaymentProvider());
PaymentService.addProvider('wallet', new WalletPaymentProvider(10));

PaymentService.pay(10, 'dummy');
PaymentService.pay(10, 'wallet');
PaymentService.pay(10, 'wallet');
